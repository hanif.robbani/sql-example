CREATE TABLE public.employee
(
    nik character varying(10) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    start_year int,
    CONSTRAINT employee_pkey PRIMARY KEY (nik)
)

TABLESPACE pg_default;

CREATE TABLE public.leave
(
    id serial NOT NULL,
    employee_nik character varying(10) COLLATE pg_catalog."default",
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    CONSTRAINT leave_pkey PRIMARY KEY (id),
    CONSTRAINT fk_leave_employee
     FOREIGN KEY (employee_nik) 
     REFERENCES employee (nik)
)

TABLESPACE pg_default;

-- insert sample data
INSERT INTO public.employee(
	nik, name, start_year)
	VALUES ('900080091', 'Agus', 2018);
INSERT INTO public.employee(
	nik, name, start_year)
	VALUES ('900080092', 'Hendra', 2017);

INSERT INTO public.leave(
	employee_nik, start_date, end_date)
	VALUES ('900080091', '2020-11-01', '2020-11-02');
INSERT INTO public.leave(
	employee_nik, start_date, end_date)
	VALUES ('900080091', '2020-11-15', '2020-11-17');
INSERT INTO public.leave(
	employee_nik, start_date, end_date)
	VALUES ('900080092', '2020-11-27', '2020-11-30');

-- schema + data in sqlfiddle:	
-- http://sqlfiddle.com/#!17/997b9